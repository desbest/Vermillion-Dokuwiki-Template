# Vermillion dokuwiki template

* Based on a wordpress theme
* Designed by [Benoit Burgener](https://wordpress.org/themes/vermillon/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:vermillion)

![vermillion theme screenshot](https://i.imgur.com/keWL78c.png)