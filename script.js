// template-related scripts go here... 
jQuery(document).ready(function(){
	/* Resize too large images */
	var size = 590;
	var image = jQuery('#content img');
	
	for (i=0; i<image.length; i++) {
		var bigWidth = image[i].width;
		var bigHeight = image[i].height;
	
		if (bigWidth > size) {	
			var newHeight = bigHeight*size/bigWidth;
			image[i].width = size;
			image[i].height = newHeight;
		}
	}
	
	jQuery("#content a:has(img)").css({'padding': '0', 'background': 'none'});
	jQuery("#s:visible").focus();
	if (jQuery("#archives").is(':visible')) {
		jQuery("#toggleArchives").html("&uarr; Menu &uarr;");
	}
});

function toggleArchives() {
	if (jQuery("#archives").is(':visible')) {
		jQuery("#archives").slideUp();
		jQuery("#toggleArchives").html("&darr; Menu &darr;");
	}
	else {
		jQuery("#toggleArchives").html("&uarr; Menu &uarr;");
		jQuery("#archives").slideDown(function() {
			jQuery("#s").focus();
		});
	}
}


// https://gitlab.com/-/snippets/2071984
jQuery(window).load(function() { // web page has finished 

    function isMobile() {
    var index = navigator.appVersion.indexOf("Mobile"); // detect chrome for android and ios safari
    var index2 = navigator.appVersion.indexOf("Android"); // detect firefox for android
    if (index2) { return (index2 > -1); }
    return (index > -1);
    }   

    usingmobile = isMobile();
    if (usingmobile){
        jQuery('input').filter(function(){
        return !jQuery(this).attr('size');
        }).attr('size',''); // change to a number if desired
        jQuery('textarea').filter(function(){
        return !jQuery(this).attr('size');
        }).attr('cols',''); // change to a number if desired

        jQuery("#edit__summary").attr('size', '');
    }
       
});

